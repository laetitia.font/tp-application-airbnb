<?php

namespace App\Controllers;

use PDOException;

use Laminas\Diactoros\ServerRequest;


use App\Models\User;
use App\Repositories\RepositoryManager;
use App\View;

class PageController
{
	/**
	 * Site Home page
	 */
	public function index(): void
	{
		$view = new View( 'pages\home' );

		$view_data = [
			'html_title' => 'AirBnb - Accueil',
			'ad' => RepositoryManager::getRm()->getAdRepository()->findAll()
		];

		$view->render( $view_data );


		// $view->render([
		// 	'html_title' => 'AirBnb - Accueil'
		// ]);
	}

	/**
	 * Pages Utilisateurs
	 */

	public function user_access(): void
	{
		$view = new View( 'pages\user_access' );

		$view_data = [
			'html_title' => 'AirBnB - Logements',
			'ad' => RepositoryManager::getRm()->getAdRepository()->findAll()
		];

		$view->render( $view_data );
	}

	public function user_booking(): void
	{

			if( View::isAuth() && View::authUser()->role == 1) {
	
				$booking = RepositoryManager::getRm()->getBookingRepository()->findByUserId( View::authUser()->id );
	
				$view = new View( 'pages\user_booking' );
				
				$view->render([
					'html_title' => 'AirBnb - Mes réservations',
					'booking' => $booking
				]);
			}
	
			else {
				View::render404();
			}
	}

	public function user_current_ad(): void
	{
		$view = new View( 'pages\user_current_ad' );

		$view_data = [
			'html_title' => 'AirBnB - Annonce',
			'ad' => RepositoryManager::getRm()->getAdRepository()->findByUserId( View::authUser()->id )
		];

		$view->render( $view_data );
	}

	public function show( int $id ): void
	{
		$ad = RepositoryManager::getRm()->getAdRepository()->findByIdAd( $id );

		// // Si la chambre n'existe pas on lance la page 404
		if( is_null( $ad ) ) {
			View::render404();
			return;
		}

		$view = new View( 'pages\user_current_ad' );
		$view->render([
			'html_title' => $ad['address'],
			'ad' => $ad
		]);
	}

	 /**
	  * Pages Annonceurs
	  */

	public function advertiser_access(): void
	{
		$view = new View( 'pages\advertiser_access' );

		$view->render([
			'html_title' => 'AirBnb - Profil'
		]);

		
	}
	
	public function advertiser_booking(): void
	{

		$booking = RepositoryManager::getRm()->getBookingRepository()->findByAdvertiserId(  View::authUser()->id );

		// // Si la chambre n'existe pas on lance la page 404
		if( is_null( $booking ) ) {
			View::render404();
			return;
		}

		$view = new View( 'pages\advertiser_booking' );
		$view->render([
			'html_title' => 'AirBnb - Réservations',
			'booking' => $booking
		]);

	}

	public function my_ads( ): void
	{
		if( View::isAuth() && View::authUser()->role == 0) {

			$ad = RepositoryManager::getRm()->getAdRepository()->findByUserId( View::authUser()->id );

			$view = new View( 'pages\advertiser_access' );
			
			$view->render([
				'html_title' => 'AirBnb - Liste de vos annonces',
				'ad' => $ad
			]);

		}

		else {
			View::render404();
		}
	}

	public function add_housing(): void
	{
		$view = new View( 'pages\add_housing' );

		$view->render([
			'html_title' => 'AirBnb - Ajout chambre'
		]);
	}

	/**
	 * Page Login
	 */
	public function login(): void
	{
		$view = new View( 'pages\login' );

		$view->render([
			'html_title' => 'AirBnb - Se connecter'
		]);
	}

	/**
	 * Login: Traitement POST
	 */
	public function processLogin( ServerRequest $request ): void
	{
		$post_data = $request->getParsedBody();

		// TODO_normaly: contrôler la saisie (saisie vide, format d'email, etc.)
		$success = RepositoryManager::getRm()->getUserRepository()->auth( $post_data[ 'login' ], $post_data[ 'password' ] );

		if( ! $success ) {
			header( 'Location: /login' );
			die();
		}

		header( 'Location: /' );
	}

	public function logout(): void
	{
		// Demande au navigateur de périmer le cookie de session
		// ini_get() récupère une configuration de php.ini
		if( ini_get('session.use_cookies') ) {
			$params = session_get_cookie_params();

			setcookie(
				session_name(),
				'',
				time() - 42000,
				$params['path'],
				$params['domain'],
				$params['secure'],
				$params['httponly']
			);
		}

		// Efface la session sur le serveur
		session_destroy();

		// Demande au navigateur une redirection vers l'accueil
		header( 'Location: /' );
		die();
	}

	/**
	 * Page inscription
	 */
	public function registration(): void
	{
		$view = new View( 'pages\registration' );

		$view->render([
			'html_title' => 'AirBnb - S\'inscrire'
		]);
	}

	// TODO: fonction a faire pour envoyer le formulaire d'inscription
	// public function processRegistration( ServerRequest $request ): void
	// {
	// 	$post_data = $request->getParsedBody();

	// 	// TODO_normaly: contrôler la saisie (saisie vide, format d'email, etc.)
	// 	$success = RepositoryManager::getRm()->getUserRepository()->auth( $post_data[ 'login' ], $post_data[ 'password' ] );

	// 	if( ! $success ) {
	// 		header( 'Location: /login' );
	// 		die();
	// 	}

	// 	header( 'Location: /' );
	// }
	// public function processRegistration( ServerRequest $request ): void
	// {
	// 	$post_data = $request->getParsedBody();

	// 	// TODO_normaly: contrôler la saisie (saisie vide, format d'email, etc.)
	// 	$success = RepositoryManager::getRm()->getUserRepository()->auth( $post_data[ 'login' ], $post_data[ 'password' ] );

	// 	if( ! $success ) {
	// 		header( 'Location: /login' );
	// 		die();
	// 	}

	// 	header( 'Location: /' );
	// }

	// public function add_user( ServerRequest $request ): void {
	// 	$post_data = $request->getParsedBody();
	// 	$user = new User();
	// 	$user->date_start = $post_data['date_start'];
	// 	$user->date_end = $post_data['date_end'];
	// 	$user->id_room = $post_data['id_room'];
	// 	$user->id_user = $post_data['id_user'];

		
	// 	// Utilisation de create
	// 	$created_booking = null;

	// 	try {
	// 		$created_booking = RepositoryManager::getRm()->getBookingRepository()->create( $booking );
	// 	}
	// 	catch( PDOException $e ) {
	// 		var_dump( $e );
	// 		View::render500();
	// 	}

	// 	header( 'Location: /chambres/' . $post_data['id_room'] );
	// }
}