<?php

namespace App\Repositories;

class RepositoryManager
{
	private static ?self $instance = null;

	private UserRepository $userRepository;
	public function getUserRepository(): UserRepository { return $this->userRepository; }

	private AdRepository $adRepository;
	public function getAdRepository(): AdRepository { return $this->adRepository; }

	private BookingRepository $bookingRepository;
	public function getBookingRepository(): BookingRepository { return $this->bookingRepository; }


	public static function getRm(): self
	{
		if( is_null(self::$instance) ) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	private function __construct()
	{
		$this->userRepository = new UserRepository();
		$this->adRepository = new AdRepository();
		$this->bookingRepository = new BookingRepository();

	}

	private function __clone() { }
	private function __wakeup() { }
}