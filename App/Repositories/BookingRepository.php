<?php

namespace App\Repositories;

use Exception;

use App\Models\Booking;

class BookingRepository extends Repository
{
	public function getColumns(): array
	{
		return [ 'id_housing', 'id_user', 'start_date', 'end_date' ];
	}


	public function getTable(): string
	{
		return 'booking';
	}

	// cRud - READ - Toutes les annonces en données brutes de la table "booking"
	public function findAll( array $query_addons = [], array $addon_data = [] ): array
	{
		return $this->readAll( Booking::class, $query_addons, $addon_data );
	}

	// cRud - READ - Un logement en données brutes de la table "bookings"
	public function findById( int $id ): ?Booking
	{
		return $this->readById( Booking::class, $id );
	}

    public function findByUserId( int $id_user ): array
    {
        $result = [];

        $q = 'SELECT 
        b.id,
        b.start_date,
        b.end_date,
        b.id_housing,
        ad.address
        -- u.login
        FROM booking AS b
                JOIN ad ON b.id_housing = ad.id
                -- JOIN users AS u ON b.user = u.id
                WHERE b.id_user=:id_user';


        
        $stmt = $this->pdo->prepare( $q );


			if( !$stmt ) {
                throw new Exception( 'Une erreur s\'est produite' );
            }
            else {
                $stmt->execute( [ 'id_user' => $id_user ] );
    
                while( $data = $stmt->fetch() ) {
                    // new $class_name() instancie une classe dont le nom est contenu dans $class_name
                    array_push( $result, $data);
                    
                }
		}

		return $result;
    }

    public function findByAdvertiserId( int $id_advertiser ): array
    {

        $result = [];

        $q = 'SELECT 
        b.id,
        b.start_date,
        b.end_date,
        b.id_housing,
        ad.address,
        u.login
        FROM booking AS b
                JOIN ad ON b.id_housing = ad.id
                JOIN users AS u ON b.id_user = u.id
                WHERE ad.id_advertiser=:id_advertiser';


        
        $stmt = $this->pdo->prepare( $q );


			if( !$stmt ) {
                throw new Exception( 'Une erreur s\'est produite' );
            }
            else {
                $stmt->execute( [ 'id_advertiser' => $id_advertiser ] );
    
                while( $data = $stmt->fetch() ) {

                    array_push( $result, $data);
                    
                }
		}

		return $result;
    }

}