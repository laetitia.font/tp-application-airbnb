<?php

namespace App\Repositories;

use Exception;

use App\Models\Equipment;

class EquipmentRepository extends Repository
{
	public function getColumns(): array
	{
		return [ 'id_ad', 'id_type_equipment' ];
	}

	public function getTable(): string
	{
		return 'equipment';
	}

	// cRud - READ - Tous les logements en données brutes de la table "equipment"
	public function findAll( array $query_addons = [], array $addon_data = [] ): array
	{
		return $this->readAll( Equipment::class, $query_addons, $addon_data );
	}

	// cRud - READ - Un logement en données brutes de la table "equipment"
	public function findById( int $id ): ?Equipment
	{
		return $this->readById( Equipment::class, $id );
	}

    // public function findByUserId( int $id_advertiser, array $query_addons = [] ): array
    // {

    //     $model_class = Equipment::class;

    //     $result = [];

    //     $q = 'SELECT * FROM '. $this->getTable() .' WHERE id_type_equipment=:id_advertiser';

    //     $stmt = $this->pdo->prepare( $q );


	// 		if( !$stmt ) {
    //             throw new Exception( 'Une erreur s\'est produite' );
    //         }
    //         else {
    //             $stmt->execute( [ 'id_advertiser' => $id_advertiser ] );
    
    //             while( $data = $stmt->fetch() ) {
    //                 // new $class_name() instancie une classe dont le nom est contenu dans $class_name
    //                 array_push( $result, new $model_class( $data ) );
    //             }
	// 	}
	// 	return $result;
    // }
	
}