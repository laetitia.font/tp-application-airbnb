<?php

namespace App\Repositories;

use Exception;

use App\Models\Ad;

class AdRepository extends Repository
{
	public function getColumns(): array
	{
		return ['id_advertiser','address', 'price', 'id_housing_type', 'size', 'description', 'number_beds' ];
	}

	public function getTable(): string
	{
		return 'ad';
	}

	// cRud - READ - Tous les logements en données brutes de la table "Ad"
	public function findAll( array $query_addons = [], array $addon_data = [] ): array
	{
		return $this->readAll( Ad::class, $query_addons, $addon_data );
	}

	// cRud - READ - Un logement en données brutes de la table "ad"
	public function findById( int $id ): ?Ad
	{
        return $this->readById( Ad::class, $id );
    }

    public function findByUserId( int $id_advertiser, array $query_addons = [] ): array
    {

        $result = [];

        // $q = 'SELECT * FROM '. $this->getTable() .' WHERE id_advertiser=:id_advertiser';

		$q = 'SELECT 
        a.id,
        a.address,
        a.price,
        a.size,
        a.description,
        a.number_beds,
		h.housing_type
        FROM ad AS a
                JOIN housing_type AS h ON a.id_housing_type = h.id
                WHERE id_advertiser=:id_advertiser';


        $stmt = $this->pdo->prepare( $q );


			if( !$stmt ) {
                throw new Exception( 'Une erreur s\'est produite' );
            }
            else {
                $stmt->execute( [ 'id_advertiser' => $id_advertiser ] );
    
                while( $data = $stmt->fetch() ) {
                    // new $class_name() instancie une classe dont le nom est contenu dans $class_name
                    array_push( $result, $data );
                }
		}
		return $result;
    }
	
    public function findByIdAd( int $id ): ?array
    {
        $result = [];
		$q = 'SELECT 
        a.id,
        a.address,
        a.price,
        a.size,
        a.description,
        a.number_beds,
		h.housing_type
        FROM ad AS a
                JOIN housing_type AS h ON a.id_housing_type = h.id
                WHERE a.id=:id';


        $stmt = $this->pdo->prepare( $q );


			if( !$stmt ) {
                throw new Exception( 'Une erreur s\'est produite' );
            }
            else {
                $stmt->execute( [ 'id' => $id ] );
    
                while( $data = $stmt->fetch() ) {
                    // new $class_name() instancie une classe dont le nom est contenu dans $class_name
                    $result = $data ;
                }
		}
		return $result;

    }
}