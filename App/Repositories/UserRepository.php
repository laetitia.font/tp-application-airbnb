<?php

namespace App\Repositories;

use App\Models\User;

class UserRepository extends Repository
{
	public function getColumns(): array
	{
		return [ 'first_name', 'last_name', 'address', 'phone', 'mail', 'login', 'password', 'role' ];
	}


	public function getTable(): string
	{
		return 'users';
	}

	public function findAll(): array
	{
		return $this->readAll( User::class );
	}

	public function findById( int $id ): ?User
	{
		return $this->readById( User::class, $id );
	}

	public function auth( string $login, string $password ): bool
	{
		$q = 'SELECT * FROM '. $this->getTable() . ' WHERE login=:login AND password=:password';

		$stmt = $this->pdo->prepare( $q );

		if( !$stmt ) {
			$_SESSION[ 'LOGIN_ERROR' ] = 'Une erreur s\'est produite';
			return false;
		}

		$stmt->execute([
			'login' => $login,
			'password' => User::hashPassword( $password )
		]);

		$data = $stmt->fetch();

		if( ! $data ) {
			$_SESSION[ 'LOGIN_ERROR' ] = 'Email ou mot de passe incorrect(s)';
			return false;
		}

		$user = new User( $data );

		$_SESSION[ 'USER' ] = $user;

		return true;
	}

}