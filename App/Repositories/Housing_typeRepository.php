<?php

namespace App\Repositories;

use Exception;

use App\Models\Housing_type;

class Housing_typeRepository extends Repository
{
	public function getColumns(): array
	{
		return [ 'housing_type' ];
	}


	public function getTable(): string
	{
		return 'housing_type';
	}

	// cRud - READ - Tous les logements en données brutes de la table "housing_type"
	public function findAll( array $query_addons = [], array $addon_data = [] ): array
	{
		return $this->readAll( Housing_type::class, $query_addons, $addon_data );
	}

	// cRud - READ - Un logement en données brutes de la table "housing_type"
	public function findById( int $id ): ?Housing_type
	{
		return $this->readById( Housing_type::class, $id );
	}

    // public function findByUserId( int $id_advertiser, array $query_addons = [] ): array
    // {

    //     $model_class = Equipment::class;

    //     $result = [];

    //     $q = 'SELECT * FROM '. $this->getTable() .' WHERE id_type_equipment=:id_advertiser';

    //     $stmt = $this->pdo->prepare( $q );


	// 		if( !$stmt ) {
    //             throw new Exception( 'Une erreur s\'est produite' );
    //         }
    //         else {
    //             $stmt->execute( [ 'id_advertiser' => $id_advertiser ] );
    
    //             while( $data = $stmt->fetch() ) {
    //                 // new $class_name() instancie une classe dont le nom est contenu dans $class_name
    //                 array_push( $result, new $model_class( $data ) );
    //             }
	// 	}
	// 	return $result;
    // }
	
}