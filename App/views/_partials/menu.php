<!-- TODO: Faire Menu advertiser -->

<?php if( !is_null( $menu_items ) && count( $menu_items ) > 0 ): ?>
    <nav>
        <?php foreach( $menu_items as $idx => $item ): ?>
            <?php if( $idx > 0 ): ?>
                <span> - </span>
            <?php endif; ?>

            <a href="<?php echo $item['filename'] ?>"><?php echo $item['label'] ?></a>
            <?php
            ?>
        <?php endforeach; ?>
    </nav>
<?php endif; ?>