<!doctype html>
<html lang="fr">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title><?php echo $html_title ?></title>

    <!-- Icones Fontawesome -->
    <link rel="stylesheet" href="/vendors/fontawesome-free-5.15.2-web/css/all.min.css">

    <!-- Bootstrap v5 -->
    <link rel="stylesheet" href="/vendors/bootstrap-5.0.0-beta2-dist/css/bootstrap.min.css">

    <!-- CSS -->
    <link rel="stylesheet" href="/css/reset.css">
    <link rel="stylesheet" href="/css/style.css">
</head>
<body>
	<main>
		<div class="heading">
			<div class="logo">
				<img src="/img/tn_house.jpg" class="img-fluid" alt="Logo">
			</div>

			<div class="our-name">
				<h1>AirBnb - Les meilleures locations du net</h1>
			</div>

            <a href="/connection">
                <div id="connection">
                        <svg xmlns="http://www.w3.org/2000/svg" width="100" height="100" fill="currentColor" class="bi bi-person-fill" viewBox="0 0 16 16">
                            <path d="M3 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H3zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
                        </svg>
                </div>
            </a>
        </div>

        <div>
			<?php if( self::isAuth() ): ?>
				Bonjour, <?php echo self::authUser()->login ?> - <a href="/deconnexion">Se déconnecter</a>
			<?php endif; ?>
		</div>

<!-- Menu USER, ADVERTISER ou simple selon le type de connection -->
<?php
if( self::isAuth() ){ 

    if(  self::authUser()->role == 0 )
    {
        $menu_items = [
            [
                'filename' => '/',
                'label' => 'Accueil'
            ],
            [
                'filename' => '/profil',
                'label' => 'Profil'
            ],
            [
                'filename' => '/reservations',
                'label' => 'Réservations'
            ],
            [
                'filename' => '/ajout',
                'label' => 'Ajout chambre'
            ]      
        ];
    

        } else if( self::authUser()->role == 1 ) {

            $menu_items = [
                [
                    'filename' => '/',
                    'label' => 'Accueil'
                ],
                [
                    'filename' => '/logements',
                    'label' => 'Nos logements'
                ],
                [
                    'filename' => '/mesreservations',
                    'label' => 'Mes réservations'
                ]        
            ];
    

    } 
}
else {

    $menu_items = [
        [
            'filename' => '/',
            'label' => 'Accueil'
        ],
        [
            'filename' => 'inscription',
            'label' => 'S\'inscrire'
        ]
    ];

}

require_once 'menu.php';

?>

<section class="page">