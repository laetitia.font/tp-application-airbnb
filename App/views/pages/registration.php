<h2>S'inscrire</h2>

<form class="row g-3">
    <div class="col-12">
        <label for="first_name" class="form-label">Prénom</label>
        <input type="text" class="form-control" id="first_name">
    </div>
    
    <div class="col-12">
        <label for="last_name" class="form-label">NOM</label>
        <input type="text" class="form-control" id="last_name">
    </div>

    <div class="col-12">
        <label for="address" class="form-label">Adresse</label>
        <input type="text" class="form-control" id="address">
    </div>

    <div class="col-md-6">
        <label for="phone" class="form-label">Téléphone</label>
        <input type="text" class="form-control" id="phone">
    </div>

    <div class="col-md-6">
      <label for="mail" class="form-label">E-mail</label>
      <input type="email" class="form-control" id="mail">
    </div>

    <div class="col-md-6">
        <label for="login" class="form-label">Choisissez un identifiant</label>
        <input type="text" class="form-control" id="login">
    </div>
    
    <div class="col-md-6">
      <label for="password" class="form-label">Mot de passe</label>
      <input type="password" class="form-control" id="password">
    </div>

    <div class="form-check">
      <input class="form-check-input" type="radio" name="role" id="advertiser" value="advertiser">
      <label class="form-check-label" for="role">
      Annonceur
      </label>
    </div>
  
    <div class="form-check">
      <input class="form-check-input" type="radio" name="role" id="user" value="user">
      <label class="form-check-label" for="role">
      Utilisateur
      </label>
    </div>

    <div class="col-12">
      <button type="submit" class="btn btn-primary">S'inscrire</button>
    </div>
</form>