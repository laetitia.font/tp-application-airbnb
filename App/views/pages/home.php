<h2>Nouveautés</h2>
<p>
	<h3>Nos derniers logements</h3>
</p>

<?php if( empty( $ad ) ): ?>
	<div><h4>Aucun logement trouvé :'(</h4></div>
<?php else: ?>
	<ul>
		<?php foreach( $ad as $housing ): ?>
			<li> 
					<a href="/annonce/<?php echo $housing->id ?>">
						<?php echo $housing->address ?>
					</a>
				<p> <?php echo $housing->description ?> </p>
				<p> <?php echo $housing->number_beds ?> couchages </p>
				<p> <?php echo $housing->price ?> €</p>
				<p> <?php echo $housing->size ?> m²</p>
                
			</li>
		<?php endforeach; ?>
	</ul>
<?php endif; ?>
