<h2>Connection</h2>

<form method="post" action="" novalidate>
  <div class="mb-3">
    <label for="identifiant" class="form-label">Votre identifiant</label>
    <input type="text" class="form-control" name="login" id="identifiant" aria-describedby="emailHelp">
  </div>

  <div class="mb-3">
    <label for="Password" class="form-label">Mot de passe</label>
    <input type="password" class="form-control" name="password" id="Password">
  </div>

  <button type="submit" class="btn btn-primary">Se connecter</button>

</form>