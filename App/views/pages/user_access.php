<h2>Nos logements</h2>


<?php if( empty( $ad ) ): ?>
	<div>Aucun logement trouvé :'(</div>
<?php else: ?>
	<ul>
		<?php foreach( $ad as $housing ): ?>
			<li> 
					<a href="/annonce/<?php echo $housing->id ?>">
						<?php echo $housing->address ?>
					</a>
				<p> <?php echo $housing->description ?> </p>
				<p> <?php echo $housing->number_beds ?> couchages </p>
				<p> <?php echo $housing->price ?> €</p>
				<p> <?php echo $housing->size ?> m²</p>
                
			</li>
		<?php endforeach; ?>
	</ul>
<?php endif; ?>
