<h2>Réservations</h2>

<?php if( empty( $booking ) ): ?>
	<div>Aucune réservation trouvée</div>
<?php else: ?>
	<ul>
		<?php foreach( $booking as $reserv ): ?>
			<li>
				<h2>
					<a href="/annonce/<?php echo $reserv['id_housing']?>"><?php echo $reserv['address']?></a>
				</h2>
				<p> Date de réservation : du <?php echo $reserv['start_date'] ?> au
				<?php echo $reserv['end_date'] ?>
                <p>Identifiant client : <?php echo $reserv['login']?></p>
            
			</li>
		<?php endforeach; ?>
	</ul>
<?php endif; ?>