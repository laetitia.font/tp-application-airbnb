<h2>Ajouter un logement</h2>


<form class="row g-3">
  <div class="col-12">
    <label for="inputAddress" class="form-label">Adresse logement</label>
    <input type="text" class="form-control" id="inputAddress" placeholder="Adresse">
  </div>

  <div class="col-12">
    <label for="price" class="form-label">Prix</label>
    <input type="number" class="form-control" id="price" placeholder="Prix">
  </div>

  <div class="col-12">
    <label for="size" class="form-label">Taille en m²</label>
    <input type="number" class="form-control" value="size" id="size" placeholder="Taille">
  </div>

  <div class="col-12">
    <label for="nber_beds" class="form-label">Nombre de couchage</label>
    <input type="number" class="form-control" value="nber_beds" id="nber_beds" placeholder="Nombre de couchage">
  </div>

  <div class="form-check">
    <input class="form-check-input" type="radio" name="type" id="entire_housing" value="entire_housing" checked>
    <label class="form-check-label" for="type">
    Logement entier
    </label>
  </div>

  <div class="form-check">
    <input class="form-check-input" type="radio" name="type" id="private_room" value="private_room">
    <label class="form-check-label" for="private_room">
    Chambre privée
    </label>
  </div>
  
  <div class="form-check">
    <input class="form-check-input" type="radio" name="type" id="shared_room" value="shared_room">
    <label class="form-check-label" for="shared_room">
    Chambre partagée
    </label>
  </div>

  <fieldset>
      <h3>Equipements</h3>
      <div class="form-check">
        <input class="form-check-input" type="checkbox"  id="toaster">
        <label class="form-check-label" for="toaster">
        Grille pain
        </label>
      </div>

      <div class="form-check">
        <input class="form-check-input" type="checkbox" id="washing_machine">
        <label class="form-check-label" for="washing_machine">
        Machine à laver
        </label>
    </div>
  </fieldset>

  </div>
    <div class="col-12">
    <button type="submit" class="btn btn-primary">Ajouter</button>
  </div>

</form>

