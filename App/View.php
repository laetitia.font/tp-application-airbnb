<?php

namespace App;

use App\Models\User;

class View
{
	private const VIEW_PATH = APP_ROOT . 'App' . DS . 'views' . DS;
	private const PARTIALS_PATH = self::VIEW_PATH . '_partials' . DS;

	private string $template_name;

	public static function render403(): void
	{
		http_response_code( 403 );

		$view = new self( '403' );
		$view->render( [ 'html_title' => 'Interdit!' ] );
		die();
	}

	public static function render404(): void
	{
		http_response_code( 404 );

		$view = new self( '404' );
		$view->render( [ 'html_title' => 'Page not found' ] );
		die();
	}

	public static function render500(): void
	{
		http_response_code( 500 );

		$view = new self( '500' );
		$view->render( [ 'html_title' => 'Server error' ] );
		die();
	}

	/**
	 * View constructor.
	 * @param string $template Template name defined by it's path starting from the "views" folder (ex: "pages\home")
	 */
	public function __construct( string $template )
	{
		$this->template_name = $template;
	}

	public function render( array $view_data = [] ): void
	{
		extract( $view_data );

		$template_path = str_replace( '\\', DS, $this->template_name );

		require_once self::PARTIALS_PATH . 'header.php';
		require_once self::VIEW_PATH . $template_path . '.php';
		require_once self::PARTIALS_PATH . 'footer.php';
	}

	public static function authUser(): ?User
	{
		return User::fromSession();
	}

	public static function isAuth(): bool
	{
		return User::isAuth();
	}


}