<?php

namespace App\Models;

use App\Repositories\RepositoryManager;

class Booking extends Model
{
	// Propriétés issues des colonnes de la table "booking"
	public int $id_housing;
	public int $id_user;
	public string $start_date;
	public string $end_date;

}