<?php

namespace App\Models;

class Ad extends Model
{
	// Propriétés issues des colonnes de la table "ad"
	public int $id_advertiser;
	public string $address;
	public float $price;
	public int $id_housing_type;
	public int $size;
    public string $description;
	public int $number_beds;

}