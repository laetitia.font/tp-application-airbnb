-- MySQL dump 10.13  Distrib 8.0.24, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: lamp
-- ------------------------------------------------------
-- Server version	5.7.29

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ad`
--

DROP TABLE IF EXISTS `ad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ad` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_advertiser` int(10) unsigned NOT NULL,
  `address` varchar(50) DEFAULT NULL,
  `price` decimal(6,2) unsigned NOT NULL,
  `id_housing_type` int(10) unsigned NOT NULL,
  `size` int(10) unsigned DEFAULT NULL,
  `description` mediumtext,
  `number_beds` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_ad_user_idx` (`id_advertiser`),
  KEY `FK_ad_housing_type_idx` (`id_housing_type`),
  CONSTRAINT `FK_ad_housing_type` FOREIGN KEY (`id_housing_type`) REFERENCES `housing_type` (`id`),
  CONSTRAINT `FK_ad_user` FOREIGN KEY (`id_advertiser`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ad`
--

LOCK TABLES `ad` WRITE;
/*!40000 ALTER TABLE `ad` DISABLE KEYS */;
INSERT INTO `ad` VALUES (18,1,'rue de la Joie 34000 MONTPELLIER',1200.36,1,90,'Logement entier avec vue sur la mer',3),(19,1,'Place Vendome 31000 TOULOUSE',500.00,2,12,'Chambre privée dans un appartement au centre de la ville. Possibilité d\'accéder aux parties communes',1),(20,1,'Avenue de Vienne 31000 TOULOUSE',200.00,3,15,'Chambre avec 3 lits',3),(21,3,'rue de la Paix 34000 MONTPELLIER',300.00,2,10,'a visiter',1);
/*!40000 ALTER TABLE `ad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `booking`
--

DROP TABLE IF EXISTS `booking`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `booking` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_housing` int(10) unsigned NOT NULL,
  `id_user` int(10) unsigned NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_booking_user_idx` (`id_user`),
  KEY `FK_booking_ad_idx` (`id_housing`),
  CONSTRAINT `FK_booking_ad` FOREIGN KEY (`id_housing`) REFERENCES `ad` (`id`),
  CONSTRAINT `FK_booking_user` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `booking`
--

LOCK TABLES `booking` WRITE;
/*!40000 ALTER TABLE `booking` DISABLE KEYS */;
INSERT INTO `booking` VALUES (1,18,2,'2019-03-01','2019-03-06'),(2,19,2,'2020-10-12','2020-10-19'),(3,20,2,'2021-05-06','2021-05-11');
/*!40000 ALTER TABLE `booking` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `equipment`
--

DROP TABLE IF EXISTS `equipment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `equipment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_ad` int(10) unsigned NOT NULL,
  `id_type_equipment` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `FK_equipment_ad_idx` (`id_ad`),
  KEY `FK_equipment_type_of_equipment_idx` (`id_type_equipment`),
  CONSTRAINT `FK_equipment_ad` FOREIGN KEY (`id_ad`) REFERENCES `ad` (`id`),
  CONSTRAINT `FK_equipment_type_of_equipment` FOREIGN KEY (`id_type_equipment`) REFERENCES `type_of_equipment` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `equipment`
--

LOCK TABLES `equipment` WRITE;
/*!40000 ALTER TABLE `equipment` DISABLE KEYS */;
INSERT INTO `equipment` VALUES (1,18,1),(2,18,2),(3,19,1),(4,19,2),(5,20,2);
/*!40000 ALTER TABLE `equipment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `housing_type`
--

DROP TABLE IF EXISTS `housing_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `housing_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `housing_type` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `housing_type`
--

LOCK TABLES `housing_type` WRITE;
/*!40000 ALTER TABLE `housing_type` DISABLE KEYS */;
INSERT INTO `housing_type` VALUES (1,'Logement entier'),(2,'Chambre privée'),(3,'Chambre partagée');
/*!40000 ALTER TABLE `housing_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `type_of_equipment`
--

DROP TABLE IF EXISTS `type_of_equipment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `type_of_equipment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type_equipment` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `type_of_equipment`
--

LOCK TABLES `type_of_equipment` WRITE;
/*!40000 ALTER TABLE `type_of_equipment` DISABLE KEYS */;
INSERT INTO `type_of_equipment` VALUES (1,'Grille pain'),(2,'Machine à laver');
/*!40000 ALTER TABLE `type_of_equipment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(45) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `mail` varchar(50) NOT NULL,
  `login` varchar(50) NOT NULL,
  `password` varchar(128) NOT NULL,
  `role` int(1) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Catherine','BELIN','33 rue de la Glace 31000 TOULOUSE','0545658596','cath.belin@gmail.com','cath','148979cdcb053e7edf700bf0c3ff8359a9d3a38e2d9687905be3510414471f2683e4fbc3d3c122a00a15835d92d7bb3a88a74c620f474e23234808cdcf4d6431',0),(2,'Didier','GREGNON','12 rue de la Miséricorde 34000 MONTPELLIER','0462365896','did.gregnon@gmail.com','did','4b0d99afdc7553826c64c33c8416e5bc78615354d749e15bb4e2c92c2333bec662b8633a0a197abc6fa360295a9fad60d0c8dc163df1e5dfd9ea77544f0cfe14',1),(3,'Patrick','NAVAM','12 rue Belettes 34000 MONTEPELLIER','0462536699','pat.navam@gmail.com','pat','4831747a4d68f272d7a77de6c0a27e84aa9a52ac7319eeecb9f5911bef67c94f4804837b41fd5a7174a98780f45fb713fd5f2506f3eb477f3fdb991686356dd2',0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-06-06 22:36:12
